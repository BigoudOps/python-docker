FROM python:3.8.6-slim 

WORKDIR /app  

COPY . /app 

RUN pip install -r requirements.txt

EXPOSE 80 

ENV NOM hello world

CMD ["python","app.py"]
